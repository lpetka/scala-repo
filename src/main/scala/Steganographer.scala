package main.scala

object Steganographer {
  val encryptedDataLengthBits = 30
  val bytesOffset = 55

  def encryptTextData(bytes: Array[Int], text: String): Array[Int] = {
    val bytesBuffer = new Array[Int](bytes.length)
    val textLengthInBits = text.length * 8
    val encryptedMessageLengthBinaryString =
      Utils.normalizeString(textLengthInBits.toBinaryString, encryptedDataLengthBits)
    var bytesCursor = bytesOffset

    // Copy header
    Array.copy(bytes, 0, bytesBuffer, 0, bytesOffset)

    // Write message length
    for (encryptedMessageLengthBit <- encryptedMessageLengthBinaryString) {
      val significantBits = bytes(bytesCursor).toBinaryString.init
      val byteBinaryString = significantBits + encryptedMessageLengthBit
      bytesBuffer(bytesCursor) = Integer.parseInt(byteBinaryString, 2)
      bytesCursor += 1
    }

    // Write message
    val textBinaryString = Converter.stringToBinaryString(text)
    for (textBinaryStringBit <- textBinaryString) {
      val significantBits = bytes(bytesCursor).toBinaryString.init
      val byteBinaryString = significantBits + textBinaryStringBit
      bytesBuffer(bytesCursor) = Integer.parseInt(byteBinaryString, 2)
      bytesCursor += 1
    }

    // Copy tail
    Array.copy(bytes, bytesCursor, bytesBuffer, bytesCursor, bytes.length - bytesCursor)

    bytesBuffer
  }

  def decryptTextData(bytes: Array[Int]): String = {
    val data: Array[Int] = getData(bytes)
    val messageLength = getEncryptedMessageLength(data)
    val charBinaryStringBuilder = new StringBuilder

    val indexTo = encryptedDataLengthBits + messageLength
    for (bitIndex <- encryptedDataLengthBits until indexTo) {
      charBinaryStringBuilder.append(getLeastSignificantBit(data(bitIndex)))
    }

    val decryptedBytes = Converter.binaryStringToBytes(charBinaryStringBuilder.toString())
    Converter.imageToString(decryptedBytes)
  }

  def getData(bytes: Array[Int]): Array[Int] = {
    bytes drop bytesOffset
  }

  def getEncryptedMessageLength(bytes: Array[Int]): Int = {
    var messageLengthBinaryString = ""
    for (i <- 0 until encryptedDataLengthBits) {
      messageLengthBinaryString += getLeastSignificantBit(bytes(i))
    }

    Integer.parseInt(messageLengthBinaryString, 2)
  }

  def getLeastSignificantBit(byte: Int): Char = {
    byte
      .toBinaryString
      .last
  }
}
