package main.scala

object Converter {
  def imageToString(bytes: Array[Int]): String = {
    bytes
      .map(e => e.toChar)
      .mkString
  }

  def stringToImage(string: String): Array[Int] = {
    string
      .toCharArray
      .map(e => e.toInt)
  }

  def stringToBinaryString(string: String): String = {
    string
      .map(e => Utils.normalizeString(e.toBinaryString, 8))
      .mkString
  }

  def binaryStringToBytes(string: String): Array[Int] = {
    val bytesNumber = string.length / 8
    val bytesBuffer = new Array[Int](bytesNumber)
    var i = 0
    for (byteBinaryString <- string.sliding(8, 8)) {
      bytesBuffer(i) = Integer.parseInt(byteBinaryString, 2)
      i += 1
    }

    bytesBuffer
  }
}
