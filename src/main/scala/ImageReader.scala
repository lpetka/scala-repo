package main.scala

import java.io.FileOutputStream
import java.nio.file.{Files, Paths}

object ImageReader {
  def loadImage(sourcePath: String): Array[Int] = {
    val pathObject = Paths.get(sourcePath)
    val rawBytes = Files.readAllBytes(pathObject)
    val bytes = Utils.getBytes(rawBytes)

    bytes
  }

  def saveImage(sourcePath: String, bytes: Array[Int]) = {
    val rawBytes = Utils.getRawBytes(bytes)
    val out = new FileOutputStream(sourcePath)
    try {
      out.write(rawBytes)
    } finally {
      out.close()
    }
  }
}
