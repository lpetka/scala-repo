import main.scala.{Converter, ImageReader, Steganographer}

case class InputException(message: String) extends Exception

object Main {
  val example1 = "Example: hide text some_text in Lena.bmp\n"
  val example2 = "Example: reveal text in Lena.bmp\n"
  val example3 = "Example: hide image smaller_image.bmp in Lena.bmp\n"
  val example4 = "Example: reveal image smaller_image.bmp in Lena.bmp\n"
  val examples = example1 + example2 + example3 + example4

  def main(args: Array[String]) {
    parseArgs(args)
  }

  def parseArgs(args: Array[String]) {
    try {
      val mode = args(0)
      val dataType = args(1)

      val function = mode + " " + dataType
      function match {
        case "hide text" =>
          saveTextInImage(args(2), args(4))
        case "reveal text" =>
          readTextFromImage(args(3))
        case "hide image" =>
          saveImageInImage(args(2), args(4))
        case "reveal image" =>
          readImageFromImage(args(2), args(4))
      }
    } catch {
      case e: Exception =>
        println("Incorrect use of function. See examples below")
        println(examples)
        e.printStackTrace()
    }
  }

  def saveTextInImage(text: String, imageSourcePath: String) = {
    val bytes = ImageReader.loadImage(imageSourcePath)
    val encryptedBytes = Steganographer.encryptTextData(bytes, text)
    ImageReader.saveImage(imageSourcePath, encryptedBytes)
  }

  def readTextFromImage(imageSourcePath: String) = {
    val bytes = ImageReader.loadImage(imageSourcePath)
    val decryptedText = Steganographer.decryptTextData(bytes)
    println("The hidden text was: " + decryptedText)
    decryptedText
  }

  def saveImageInImage(smallerImageSourcePath: String, biggerImageSourcePath: String) = {
    val smallerImageBytes = ImageReader.loadImage(smallerImageSourcePath)
    val smallerImageString = Converter.imageToString(smallerImageBytes)
    saveTextInImage(smallerImageString, biggerImageSourcePath)
  }

  def readImageFromImage(smallerImageSourcePath: String, biggerImageSourcePath: String) = {
    val biggerImageBytes = ImageReader.loadImage(biggerImageSourcePath)
    val decryptedText = Steganographer.decryptTextData(biggerImageBytes)
    val smallerImageBytes = Converter.stringToImage(decryptedText)
    ImageReader.saveImage(smallerImageSourcePath, smallerImageBytes)
  }
}
