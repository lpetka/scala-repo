package main.scala

object Utils {
  def normalizeString(string: String, length: Int): String = {
    val zeroesNumber = length - string.length
    val normalizedString = "0" * zeroesNumber + string

    normalizedString
  }

  def getRawBytes(bytes: Array[Int]): Array[Byte] = {
    bytes.map(e => (e - 128).toByte)
  }

  def getBytes(rawBytes: Array[Byte]): Array[Int] = {
    rawBytes.map(e => e + 128)
  }
}
